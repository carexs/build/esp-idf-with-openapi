FROM registry.gitlab.com/carexs/kago/esp-idf:kago_4_2_ci_build_at_2021-07-13

RUN apt update && apt install default-jdk maven jq -y && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /lib/bin/openapitools && \
    curl https://raw.githubusercontent.com/OpenAPITools/openapi-generator/master/bin/utils/openapi-generator-cli.sh > /lib/bin/openapitools/openapi-generator-cli && \
    chmod a+x /lib/bin/openapitools/openapi-generator-cli
ENV PATH="$PATH:/lib/bin/openapitools/"

RUN /lib/bin/openapitools/openapi-generator-cli version
